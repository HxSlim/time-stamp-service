const isNumber = value => !isNaN(value);
const timestamp = date => new Date(date).valueOf();
const isValidDate = date => (isNumber(date) && isNumber(timestamp(new Number(date)))) || isNumber(timestamp(date));

const dateConverter = date => {
    const dateObject = isNumber(date) ? new Date(new Number(date)) : new Date(date);
    const convertedDate = {
        unix: dateObject.valueOf(),
        utc: dateObject.toUTCString()
    };
    return convertedDate;
};

const convertDate = dateToConvert => {
    let convertedDate = null;
    if (dateToConvert === null) {
        convertedDate = dateConverter(Date.now());
    } else if (isValidDate(dateToConvert)) {
        convertedDate = dateConverter(dateToConvert);
    }
    return convertedDate;
};

exports.convertDate = convertDate;